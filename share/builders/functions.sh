function _ask() {
  echo -n "Enter to continue/^C to break"

  read
}

function _basename() {
  local filename=$(basename "$1")
  local result=$(basename "$filename" .tar)
  if [ "$result" == "$filename" ] ; then
    result=$(basename "$filename" .tar.gz)
    if [ "$result" == "$filename" ] ; then
      result=$(basename "$filename" .tar.bz2)
      if [ "$result" == "$filename" ] ; then
        result=$(basename "$filename" .tar.xz)
        if [ "$result" == "$filename" ] ; then
          result=$(basename "$filename" .zip)
        fi
      fi
    fi
  fi
  echo "$result";
}

function _clean_build_dir() {
  if [ -d "$PKG_BUILD_DIR" -a -d "$PKG_SRC_DIR" ] ; then
    cd "$PKG_BUILD_DIR"
    local b="$PWD"

    cd "$PKG_SRC_DIR"
    local s="$PWD"

    if [[ "$s" != "$b"* ]] ; then
      if [[ "$b" != "$s"* ]] ; then
        rm -frv "$PKG_BUILD_DIR"
      fi
    fi
  fi
}

function _unarchive() {
  local filename="$1"

  if echo $filename | grep -q '\.zip$' ; then
    unzip "$filename"
  else
    tar xf "$filename"
  fi
}

function _read_props() {
  local line=""
  local lines=""
  while IFS='' read line || [[ -n "$line" ]] ; do
    lines="$lines$line "
  done < "$1"
  eval "$lines"
}

function _adjust_pkg_name_and_version() {
  local file=$(command ls -1 "$1"* 2>/dev/null)
  file=$(_basename "$file")
  
  PKG_VERSION="${file##*-}"
  PKG_NAME="${file%-*}"
  
  if [ "$PKG_VERSION" == "$file" ] ; then
    echo "Cannot identify the package version"
    exit -6
  fi
}

function _set_pkg_name_and_version() {
  PKG_NAME=$(_basename "$1")
  PKG_VERSION="${PKG_NAME##*-}"
  PKG_NAME="${PKG_NAME%-*}"

  if [ -z "$PKG_NAME" ] ; then
    echo "Cannot identify the package name from $1"
    exit -3
  fi

  if [ "$PKG_VERSION" == "$PKG_NAME" ] ; then
    local n=$(command ls -1 "$REPO_DIR/$PKG_NAME-"* 2>/dev/null | wc -l)
    if [ "$n" -eq "1" ] ; then
      _adjust_pkg_name_and_version "$REPO_DIR/$PKG_NAME-"
    elif [ "$n" -eq "0" ] ; then
      n=$(command ls -1 "$WORKDIR/share/urls/phase$PHASE/$PKG_NAME-"* 2>/dev/null | wc -l)
      if [ "$n" -eq "1" ] ; then
        _adjust_pkg_name_and_version "$WORKDIR/share/urls/phase$PHASE/$PKG_NAME-"
      else
        echo "Cannot identify the package version"
        exit -4
      fi
    else
      echo "Cannot identify the package version"
      exit -5
    fi
  fi
}

function initialize() {
  echo "STEP: initialize"

  PKG_ARCH=x86
  LIB_SUFFIX="$ARCH"
  IDLE_LD_FLAGS="$IDLE_LD_FLAGS_ARCH32"
  IDLE_C_FLAGS="$IDLE_C_FLAGS_ARCH32"
  IDLE_CXX_FLAGS="$IDLE_CXX_FLAGS_ARCH32"
  IDLE_TARGET="$IDLE_TARGET_ARCH32"
  if [ "$ARCH" -eq "64" ] ; then
    PKG_ARCH="x86_64"
    IDLE_LD_FLAGS="$IDLE_LD_FLAGS_ARCH64"
    IDLE_C_FLAGS="$IDLE_C_FLAGS_ARCH64"
    IDLE_CXX_FLAGS="$IDLE_CXX_FLAGS_ARCH64"
    IDLE_TARGET="$IDLE_TARGET_ARCH64"
  fi

  _set_pkg_name_and_version "$1"

  PKG_LONG_NAME="$PKG_NAME-$PKG_VERSION"

  if [ -r "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/boot.properties" ] ; then
    _read_props "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/boot.properties"
  fi

  PKG_SRC_FILE_NAME="$PKG_LONG_NAME.tar.xz"
  PKG_DST_FILE_NAME="$PKG_LONG_NAME-$PKG_ARCH.tar.xz"
  [ -n "$PHASE_NAME" ] && PKG_DST_FILE_NAME="$PKG_LONG_NAME-$PKG_ARCH-$PHASE_NAME.tar.xz"
  PKG_SRC_DIR="$SRC_DIR/$PKG_LONG_NAME"
  PKG_DST_DIR="$DST_DIR/$PKG_LONG_NAME"

  PKG_BUILD_DIR="$PKG_SRC_DIR"
  [ "$IDLE_CONFIG" == "mk" -o "$IDLE_CONFIG" == "qmk" ] && PKG_BUILD_DIR="$PKG_SRC_DIR/build"

  PKG_CFG_DIR="$PKG_SRC_DIR"
  PKG_TMP_DIR="${TMP:-/tmp}/idlebuilder/bld/$PKG_LONG_NAME"

  CFG="--prefix=$PREFIX \
    --libdir=$PREFIX/lib$LIB_SUFFIX \
    --sysconfdir=$IDLE_ROOT/etc \
    --localstatedir=$IDLE_ROOT/var \
    --datarootdir=$PREFIX/share \
    --docdir=$PREFIX/share/doc \
    --mandir=$PREFIX/share/man \
    --infodir=$PREFIX/share/info \
    --disable-static \
  "
  CFG_ENV="LDFLAGS='$IDLE_LD_FLAGS' CFLAGS='$IDLE_C_FLAGS' CXXFLAGS='$IDLE_CXX_FLAGS'"
  COMPILE_OPTS=${COMPILE_OPTS:-"$IDLE_COMPILE_OPTS"}
  COPY_OPTS="install DESTDIR=$PKG_DST_DIR"

  WRAP=""

  if [ -r "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/init.properties" ] ; then
    _read_props "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/init.properties"
  fi
  
  rm -fr "$PKG_TMP_DIR" >/dev/null 2>&1 || true
  mkdir -p "$PKG_TMP_DIR"
}

function dump() {
  echo "STEP: dump"

  echo WORKDIR=$WORKDIR
  echo ARCH=$ARCH
  echo PREFIX=$PREFIX
  echo IDLE_HOST=$IDLE_HOST
  echo IDLE_TARGET=$IDLE_TARGET
  echo PKG_ARCH=$PKG_ARCH
  echo PKG_NAME=$PKG_NAME
  echo PKG_VERSION=$PKG_VERSION
  echo PKG_LONG_NAME=$PKG_LONG_NAME
  echo PKG_SRC_FILE_NAME=$PKG_SRC_FILE_NAME
  echo PKG_DST_FILE_NAME=$PKG_DST_FILE_NAME
  echo PKG_SRC_DIR=$PKG_SRC_DIR
  echo PKG_DST_DIR=$PKG_DST_DIR
  echo PKG_BUILD_DIR=$PKG_BUILD_DIR
  echo PKG_CFG_DIR=$PKG_CFG_DIR
  echo PKG_TMP_DIR=$PKG_TMP_DIR
  echo CFG=$CFG
  echo CFG_ENV=$CFG_ENV
  echo COMPILE_OPTS=$COMPILE_OPTS
  echo COPY_OPTS=$COPY_OPTS

  [ -n "$WRAP" ] && echo WRAP=$WRAP

  echo
}

function aide {
  echo "STEP: aide"

  if [ "$IDLE_CONFIG" == "mk" ] ; then
    _clean_build_dir

    mkdir -p "$PKG_BUILD_DIR"
    cd "$PKG_BUILD_DIR"

    cmake -LAH "$PKG_SRC_DIR"
  elif [ "$IDLE_CONFIG" == "qmk" ] ; then
    echo "Operation [aide] is not supported"
    exit -1
  else
    "$PKG_CFG_DIR/configure" --help
  fi
}

function download() {
  echo "STEP: download"

  if [ -r "$REPO_DIR/$PHASE_NAME/$PKG_SRC_FILE_NAME" ] ; then
    return
  fi

  mkdir -p "$DOWNLOAD_DIR/$PHASE_NAME/$PKG_LONG_NAME"
  cd "$DOWNLOAD_DIR/$PHASE_NAME/$PKG_LONG_NAME"

  local n=0
  local url=""
  # #type[{!type_cmd,!!type_script_name}]#{!fetch_cmd,!!fetch_script_name,[[?file_name?]url]}
  while IFS="" read url || [[ -n "$url" ]] ; do
    if echo "$url" | grep -q -v "^[ \t]*$" ; then
      local type=$(echo $url | sed -r 's,^#([^#]+)#.*,\1,')
      local type_cmd=$(echo $type | grep '[^!]![^!]' | sed -r 's,([^!]+)(!([^!].*))?,\3,')
      local type_script_name=$(echo $type | grep '!!' | sed -r 's,([^!]+)(!!(.+))?,\3,')
      type=$(echo $type | sed -r 's,([^!]+)!.+,\1,')

      if [ "$type" == "archive" ] ; then
        if [ "$n" -ne "0" ] ; then
          echo "Too many archives"
          exit -7
        fi
        n=$[n+1]

        rm -fr "$DOWNLOAD_DIR/$PHASE_NAME/$PKG_LONG_NAME/$type/$PKG_LONG_NAME" >/dev/null 2>&1 || true
        if [ -d "$DOWNLOAD_DIR/$PHASE_NAME/$PKG_LONG_NAME/$type/$PKG_LONG_NAME" ] ; then
          echo "Directory still exists $DOWNLOAD_DIR/$PHASE_NAME/$PKG_LONG_NAME/$type/$PKG_LONG_NAME"
          exit -8
        fi
      elif [ "$type" == "patch" ] ; then
        echo
      elif [ "$type" == "artifact" ] ; then
        echo
      else
        echo "Unknown type $type"
        exit -9
      fi

      mkdir -p "$DOWNLOAD_DIR/$PHASE_NAME/$PKG_LONG_NAME/$type" >/dev/null 2>&1 || true
      cd "$DOWNLOAD_DIR/$PHASE_NAME/$PKG_LONG_NAME/$type"

      url=$(echo $url | sed -r 's,^#[^#]+#(.*),\1,')
      if echo $url | grep -q '^!!' ; then
        local fetch_script_name=$(echo $url | sed -r 's,!!(.+),\1,')

        echo STEP: download: fetch script:
        echo $WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/$fetch_script_name

        . "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/$fetch_script_name"
      elif echo $url | grep -q '^!' ; then
        local fetch_cmd=$(echo $url | sed -r 's,!([^!].*),\1,')

        echo STEP: download: fetch custom command:
        echo $fetch_cmd

        eval "$fetch_cmd"
      else
        if echo $url | grep -q '^?' ; then
          local file_name=$(echo $url | sed -r 's,^\?([^\?]+)\?.+,\1,')
          if [ ! -r "$file_name" ] ; then
            url=$(echo $url | sed -r 's,^\?[^\?]+\?(.+),\1,')

            echo STEP: download: fetch command:
            echo wget"$url" -O "$file_name"

            wget "$url" -O "$file_name"
          fi
        elif [ -n "$url" ] ; then
          echo STEP: download: fetch command:
          echo wget -N "$url"

          wget -N "$url"
        fi
      fi

      if [ -n "$type_script_name" ] ; then
        echo STEP: download: type script name:
        echo $WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/$type_script_name

        . "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/$type_script_name"
      elif [ -n "$type_cmd" ] ; then
        echo STEP: download: type custom command:
        echo "$type_cmd"

        eval "$type_cmd"
      else
        if [ "$type" == "archive" ] ; then
          echo STEP: download: default archive command

          local filename=$(command ls -1)
          _unarchive "$filename"

          local bn=$(find -maxdepth 1 -type d -not -path '.')
          bn=$(basename "$bn")
          if [ "$bn" != "$PKG_LONG_NAME" ] ; then
            mv "$bn" "$PKG_LONG_NAME"
          fi
        elif [ "$type" == "patch" ] ; then
          local f=""
          command ls -1 "$DOWNLOAD_DIR/$PHASE_NAME/$PKG_LONG_NAME/patch/"* | while f ; do
            echo STEP: download: default patch command:
            echo patch -d $DOWNLOAD_DIR/$PHASE_NAME/$PKG_LONG_NAME/archive/$PKG_LONG_NAME -Np 1 '<' "$f"

            patch -d "$DOWNLOAD_DIR/$PHASE_NAME/$PKG_LONG_NAME/archive/$PKG_LONG_NAME" -Np 1 < "$f"
          done
        fi
      fi
    fi
  done < "$WORKDIR/share/urls/phase$PHASE/$PKG_LONG_NAME"

  cd "$DOWNLOAD_DIR/$PHASE_NAME/$PKG_LONG_NAME/archive"
  mkdir -p "$REPO_DIR/$PHASE_NAME"
  tar cO "$PKG_LONG_NAME" | xz -9 > "$REPO_DIR/$PHASE_NAME/$PKG_SRC_FILE_NAME"
  rm -fr "$PKG_LONG_NAME"
}

function unpack() {
  echo "STEP: unpack"

  if [ -r "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/unpack" ] ; then
    . "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/unpack"
  else
    rm -fr "$PKG_SRC_DIR" 2>/dev/null || true
    tar xf "$REPO_DIR/$PHASE_NAME/$PKG_SRC_FILE_NAME" -C "$(dirname $PKG_SRC_DIR)"
  fi
}

# Patch file name may inlcude phase name, arch name or none of them:
#   package-version-x86_64-crosstools.pacthes.xz
#   package-version-x86_64.patches.xz
#   package-version.patches.xz
#
# Depending on the name, appropriate patches will be applied depending on the phase and arch.
function preconfig() {
  echo "STEP: preconfig"

  cd $PKG_SRC_DIR

  local patch_file_path=""
  if [ -n "$PHASE_NAME" ] ; then
      patch_file_path="$REPO_DIR/$PKG_LONG_NAME-$PKG_ARCH-$PHASE_NAME.patches.xz"
      [ -r "$patch_file_path" ] || patch_file_path=""
  fi
  if [ -z "$patch_file_path" ] ; then
    patch_file_path="$REPO_DIR/$PKG_LONG_NAME-$PKG_ARCH.patches.xz"
    [ -r "$patch_file_path" ] || patch_file_path=""
  fi
  if [ -z "$patch_file_path" ] ; then
    patch_file_path="$REPO_DIR/$PKG_LONG_NAME.patches.xz"
    [ -r "$patch_file_path" ] || patch_file_path=""
  fi
  if [ -n "$patch_file_path" ] ; then
    echo "Applying patches from $patch_file_path"
    unxz -c "$patch_file_path" | patch $PATCH_OPTS
  fi

  if [ -r "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/preconfig" ] ; then
    . "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/preconfig"
  fi
}

function config() {
  echo "STEP: config"

  _clean_build_dir

  mkdir -p $PKG_BUILD_DIR
  cd $PKG_BUILD_DIR

  if [ -r "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/config" ] ; then
    . "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/config"
  else
    if [ "$IDLE_CONFIG" == "mk" ] ; then
      eval "cmake $CMAKE_CFG $PKG_CFG_DIR"
    elif [ "$IDLE_CONFIG" == "qmk" ] ; then
      eval "qmake $QMAKE_CFG"
    else
      eval "$CFG_ENV $PKG_CFG_DIR/configure $CFG"
    fi
  fi
}

function customconfig() {
  cd $PKG_SRC_DIR

  if [ -r "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/customconfig" ] ; then
    echo "STEP: customconfig"

    . "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/customconfig"
  fi
}

function compile() {
  echo "STEP: compile"

  cd $PKG_BUILD_DIR
  if [ -r "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/compile" ] ; then
    echo "STEP: compile"

    . "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/compile"
  else
    make $COMPILE_OPTS
  fi
}

function postcompile() {
  cd $PKG_BUILD_DIR

  if [ -r "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/postcompile" ] ; then
    echo "STEP: postcompile"

    . "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/postcompile"
  fi
}

function copy() {
  echo "STEP: copy"

  rm -fr $PKG_DST_DIR >/dev/null 2>&1 || true
  cd $PKG_BUILD_DIR
  if [ -r "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/copy" ] ; then
    . "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/copy"
  else
    make $COPY_OPTS
  fi
}

function customcopy() {
  cd $PKG_BUILD_DIR

  if [ -r "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/customcopy" ] ; then
    echo "STEP: customcopy"

    . "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/customcopy"
  fi
}

function clean() {
  echo "STEP: clean"

  mkdir -p "$PKG_DST_DIR/$PREFIX/share"
  [ -d "$PKG_DST_DIR/$PREFIX/man" ] && mv "$PKG_DST_DIR/$PREFIX/man" "$PKG_DST_DIR/$PREFIX/share"

  rm -fr "$PKG_DST_DIR/$PREFIX/"{,share}/{info,doc,gtk-doc,applications}

  [ "$DROP_MAN" == "y" ] && rm -fr "$PKG_DST_DIR/$PREFIX/share/man"
  [ "$DROP_LOCALE" == "y" ] && rm -fr "$PKG_DST_DIR/$PREFIX/share/locale"

  if [ -d "$PKG_DST_DIR/$PREFIX/share/pkgconfig" ] ; then
    if [ -n "$(command ls -1 $PKG_DST_DIR/$PREFIX/share/pkgconfig)" ] ; then
      mkdir -p "$PKG_DST_DIR/$PREFIX/lib$LIB_SUFFIX/pkgconfig"
      cp -r "$PKG_DST_DIR/$PREFIX/share/pkgconfig/"* "$PKG_DST_DIR/$PREFIX/lib$LIB_SUFFIX/pkgconfig"
    fi
    rm -fr "$PKG_DST_DIR/$PREFIX/share/pkgconfig"
  fi

  if [ -d "$PKG_DST_DIR/$PREFIX/share" ] ; then
    [ -z "$(command ls -1 $PKG_DST_DIR/$PREFIX/share)" ] && rm -fr "$PKG_DST_DIR/$PREFIX/share"
  fi
  if [ -d "$PKG_DST_DIR/$PREFIX" ] ; then
    [ -z "$(command ls -1 $PKG_DST_DIR/$PREFIX)" ] && rm -fr "$PKG_DST_DIR/$PREFIX"
  fi

  if [ -d "$PKG_DST_DIR/lib32" ] ; then
    if [ -n "$(command ls -1 $PKG_DST_DIR/lib32)" ] ; then
      mkdir -p "$PKG_DST_DIR/$PREFIX/lib32"
      cp -r "$PKG_DST_DIR/lib32/"* "$PKG_DST_DIR/$PREFIX/lib32"
    fi
    rm -fr "$PKG_DST_DIR/lib32"
  fi
  if [ -d "$PKG_DST_DIR/lib64" ] ; then
    if [ -n "$(command ls -1 $PKG_DST_DIR/lib64)" ] ; then
      mkdir -p "$PKG_DST_DIR/$PREFIX/lib64"
      cp -r "$PKG_DST_DIR/lib64/"* "$PKG_DST_DIR/$PREFIX/lib64"
    fi
    rm -fr "$PKG_DST_DIR/lib64"
  fi
  if [ -d "$PKG_DST_DIR/$PREFIX/sbin" ] ; then
    if [ -n "$(command ls -1 $PKG_DST_DIR/$PREFIX/sbin)" ] ; then
      mkdir -p "$PKG_DST_DIR/$PREFIX/bin"
      cp -r "$PKG_DST_DIR/$PREFIX/sbin/"* "$PKG_DST_DIR/$PREFIX/bin"
    fi
    rm -fr "$PKG_DST_DIR/$PREFIX/sbin"
  fi
  if [ -d "$PKG_DST_DIR/sbin" ] ; then
    if [ -n "$(command ls -1 $PKG_DST_DIR/sbin)" ] ; then
      mkdir -p "$PKG_DST_DIR/$PREFIX/bin"
      cp -r "$PKG_DST_DIR/sbin/"* "$PKG_DST_DIR/$PREFIX/bin"
    fi
    rm -fr "$PKG_DST_DIR/sbin"
  fi
  if [ -d "$PKG_DST_DIR/bin" ] ; then
    if [ -n "$(command ls -1 $PKG_DST_DIR/bin)" ] ; then
      mkdir -p "$PKG_DST_DIR/$PREFIX/bin"
      cp -r "$PKG_DST_DIR/bin/"* "$PKG_DST_DIR/$PREFIX/bin"
    fi
    rm -fr "$PKG_DST_DIR/bin"
  fi

  if [ -d "$PKG_DST_DIR/$PREFIX/bin" ] ; then
    [ -z "$(command ls -1 $PKG_DST_DIR/$PREFIX/bin)" ] && rm -fr "$PKG_DST_DIR/$PREFIX/bin"
  fi
  if [ -d "$PKG_DST_DIR/$PREFIX/sbin" ] ; then
    [ -z "$(command ls -1 $PKG_DST_DIR/$PREFIX/sbin)" ] && rm -fr "$PKG_DST_DIR/$PREFIX/sbin"
  fi

  if [ "${NO_STRIP:-}" != "y" ] ; then
    find "$PKG_DST_DIR/$PREFIX/"{,usr/}{bin,sbin,lib,lib32,lib64} -type f -exec strip --strip-debug '{}' ';' 2>/dev/null || true
  fi

  if [ -n "${WRAP}" ] ; then
    for f in $WRAP ; do
      local fullfilename="$PKG_DST_DIR/$f"
      local filedirname="$(dirname $fullfilename)"
      local filename="$(basename $fullfilename)"

      local name="$(echo $filename | sed -r -e 's,([^.]+)(\..*)?,\1,')"
      local postfix="$(echo $filename | sed -r -e 's,([^.]+)(\..*)?,\2,')"

      mv -v $fullfilename "${filedirname}/${name}-$ARCH$postfix"
      ln -sfv multiarch_wrapper "$fullfilename"
    done
  fi
}

function customclean() {
  cd $PKG_DST_DIR

  if [ -r "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/customclean" ] ; then
    echo "STEP: customclean"

    . "$WORKDIR/share/builders/phase$PHASE/$PKG_LONG_NAME/customclean"
  fi
}

function package() {
  echo "STEP: package"

  [ -r "$DST_DIR/$PKG_DST_FILE_NAME" ] && rm "$DST_DIR/$PKG_DST_FILE_NAME"
  cd "$PKG_DST_DIR"
  tar cO * | xz -9 > "$DST_DIR/$PKG_DST_FILE_NAME"
}

function deploy() {
  echo "STEP: deploy"

  tar xf "$DST_DIR/$PKG_DST_FILE_NAME" -C "$IDLE_ROOT"
}
