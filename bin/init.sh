#!/bin/bash

set +h
set -e
set -u

cd $(dirname $0)/..

WORKDIR=$PWD
PHASE="$1"
IDLE_VERSION=`basename $WORKDIR | sed -r 's,idlebuilder-(.+),\1,'`
IDLE_RCFILE=$WORKDIR/share/phases/phase$PHASE/bash.rc

if [ -r "$IDLE_RCFILE" ] ; then
  sudo -H -u idle bash -c "env -i PATH=\"/bin:/sbin:/usr/bin:/usr/sbin\" USER=\$USER HOME=\$HOME TERM=$TERM IDLE_VERSION=$IDLE_VERSION WORKDIR=$WORKDIR PHASE=$PHASE bash --rcfile $IDLE_RCFILE"
else
  echo "Not found $IDLE_RCFILE"
fi
