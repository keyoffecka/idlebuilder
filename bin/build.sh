#!/bin/bash

set +h
set -e
set -u

source $WORKDIR/share/builders/functions.sh

case "$#" in
  0)
    echo "Wrong argument count"
    exit -1
  ;;

  1)
    initialize "$1"
    dump
    [ "${NO_ASK:-}" != "y" ] && _ask
    download "$1"
    unpack "$1"
    preconfig
    config
    customconfig
    compile
    postcompile
    copy
    customcopy
    clean
    customclean
    [ "${NO_ASK:-}" != "y" ] && _ask
    package
    deploy
  ;;

  *)
    package_name="$1"

    initialize "$package_name"

    shift

    while (( $# )); do
      if [ "$1" == "initialize" ] ; then
        echo "The 'initilize' task is always called at the very beginning and shouldn't be called explicitly."
        exit -2
      fi

      eval "$1 \"$package_name\""

      shift
    done
  ;;

esac
