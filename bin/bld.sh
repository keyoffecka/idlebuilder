#!/bin/bash

set +h
set -u
set -e

cd $(dirname $0)/..

export ARCH=$(basename $0 | sed -r 's,[^-]+(-(.+))?\.sh,\2,')
[ -z "$ARCH" ] && export ARCH="64"

export IDLE_CONFIG=$(basename $0 | sed -r 's,([^-]+)(-.+)?\.sh,\1,')

function run() {
  echo >/tmp/bld.result
  set +e
  $WORKDIR/bin/build.sh $@
  echo $?>/tmp/bld.result
  set -e
}

rm /tmp/bld.log >/dev/null 2>&1 || true
run $@ 2>&1 | tee /tmp/bld.log
_result=$(cat /tmp/bld.result)
rm /tmp/bld.result
exit $_result
